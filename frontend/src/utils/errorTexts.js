/**
 * Project-wide constants for error-texts
 */
import {UNKNOWN_ERROR,
    USERNAME_ALREADY_TAKEN,
    COULD_NOT_CREATE_ACCOUNT,
    INVALID_ID,INVALID_INPUT,
    USERNAME_DOES_NOT_EXIST,
    WRONG_PASSWORD,
    NOT_LOGGED_IN,
    CANT_DELETE_SELF} from './errorcodes'

export const errorText = (errorCode)=> {
    switch(errorCode){
        case UNKNOWN_ERROR: return 'Unknown Error.';
        case USERNAME_ALREADY_TAKEN: return 'Username already taken.';
        case COULD_NOT_CREATE_ACCOUNT: return 'Could not create account.';
        case INVALID_INPUT: return 'Invalid input.';
        case USERNAME_DOES_NOT_EXIST: return 'Username does not exist.';
        case WRONG_PASSWORD: return 'Wrong password.';
        case NOT_LOGGED_IN: return 'Not logged in.';
        case INVALID_ID: return 'Invalid ID.';
        case CANT_DELETE_SELF: return 'You cant\'t remove yourself!';
        default: 'Unkown Error ('+errorCode+')';
    }
};