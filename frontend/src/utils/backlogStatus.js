/**
 * Project-wide constants for backlog-status
 */

/**
 * Project-wide constants for error-texts
 */

export const backlogStatus = (errorCode)=> {
    switch(errorCode){
        case 0: return 'Created';
        case 1: return 'Commited';
        case 2: return 'In Progress';
        case 3: return 'Done';
        case 4: return 'Approved';
    }
};

module.exports = backlogStatus;