import {observable, action} from "mobx";
import APICalls from "../ApiCalls/APICalls";
import projectState from './ProjectState';
import errorCodes from "../utils/errorcodes";
import appState from './ErrorMessageState';

class ProjectStore {

    @observable projects;
    @observable users;

    constructor () {
        this.projects = [];
        this.users = [];
        this.updateProjects();
        this.getAllUsers();
    }

    @action updateProjects(){
        APICalls.getProjectsForUser().then((projects) => {
            if(projects.status != '200' || projects.data == ''){
                this.projects = null;
            }else{
                this.projects = projects.data;
            }
        });
    }

    @action createProject(data){
        APICalls.createProject(data).then((project) => {
            this.updateProjects();
        })
    }

    @action createSprint(data,projectId){
        APICalls.createSprint(data,projectId).then((sprint) => {
            if(sprint.status != '201'){
                appState.createErrorMessage('Cant create Sprint!', sprint.data['error-code'] || -1);
                return;
            }
            projectState.updateSprints();
        } )
    }

    @action deleteSprint(sprintId,projectId){
        APICalls.deleteSprint(sprintId,projectId).then((deleted) => {
            if(deleted.status != '200'){
                appState.createErrorMessage('Cant delete BacklogItem!', deleted.data['error-code'] || -1);
                return;
            }
            projectState.updateSprints();
        });
    };

    @action getAllUsers(){
        APICalls.getAllUsers().then((users) => {
            this.users = users.data;
        })
    }

    @action addUserToProject(projectId,userId){
        APICalls.addUserToProject(projectId,userId).then((added) => {
            if(added.status != '201'){
                appState.createErrorMessage('Cant add User to Project!', added.data['error-code'] ||
                                                                        errorCodes.UNKNOWN_ERROR);
                return;
            }
            projectState.updateMembers();
        });
    }

    @action removeUserFromProject(projectId,userId){
        APICalls.removeUserFromProject(projectId,userId).then((added) => {
            if(added.status != '200'){
                appState.createErrorMessage('Cant add User to Project!', added.data['error-code'] ||
                    errorCodes.UNKNOWN_ERROR);
                return;
            }
            projectState.updateMembers();
        });
    }

    @action deleteBacklogItem(itemId,projectId){
        APICalls.deleteBacklogItem(itemId,projectId).then((deleted) => {
            if(deleted.status != '200'){
                if(deleted.data['error-code'] != null){
                    appState.createErrorMessage('Cant delete BacklogItem!', deleted.data['error-code']);
                    return;
                }
                appState.createErrorMessage('Cant delete BacklogItem!', errorCodes.UNKNOWN_ERROR);
                return;
            }
            projectState.updateBacklogItems();
        });
    };

    @action updateBacklogItem(data,projectId,sprintKey,backlogId){
        APICalls.updateBacklogItem(data,projectId,sprintKey,backlogId).then((updated) => {
            if(updated.status != '200'){
                appState.createErrorMessage('Cant update BacklogItem!', updated.data['error-code'] || errorCodes.UNKNOWN_ERROR);
                return;
            }
            projectState.updateBacklogItems();
        });
    }

    @action updateUserProject(data,projectId,userId){
        APICalls.updateUserProject(data,projectId,userId).then((updated) => {
            if(updated.status != '200'){
                appState.createErrorMessage('Cant update User!', updated.data['error-code'] || errorCodes.UNKNOWN_ERROR);
            }
            projectState.updateMembers();
        });
    }

    @action createBacklogItem(data,projectId){
        APICalls.createBacklogItem(data,projectId,data.sprint_key).then((backlogItem) => {
            if(backlogItem.status != '201'){
                if(backlogItem.data['error-code'] != null){
                    appState.createErrorMessage('Cant create BacklogItem!', backlogItem.data['error-code']);
                    return;
                }
                appState.createErrorMessage('Cant create BacklogItem!', errorCodes.UNKNOWN_ERROR);
                return;
            }
            projectState.updateBacklogItems();
        });
    }

}

export default new ProjectStore();