import {observable, action} from "mobx";

class ManageTeamModalState {

    @observable editUser;
    @observable user;

    constructor() {
        this.editUser = false;
        this.user = {
            role:null,
            isAdmin:null
        };
    }

    @action closeEditUser(){
        this.editUser = false;
        this.user = {
            id: null,
            username: null,
            role:null,
            isAdmin:null
        };
    }

    @action openEditUser(editUser){
        this.user = editUser;
        this.editUser = true;
    }

}

export default new ManageTeamModalState();