import {computed, observable, action} from "mobx";
import APICalls from "../ApiCalls/APICalls";
import errorCodes from "../utils/errorcodes";
import AppState from './ErrorMessageState';
import HashMap from 'hashmap';

class ProjectState {

    @observable selectedProject;
    @observable selectedSprint;
    @observable sprints;
    @observable isAdmin;
    @observable backlogItems;
    @observable members;
    @observable membersHash;
    @observable sprintsHash;

    constructor () {
        this.selectedProject = {
            name: ''
        };
        this.isAdmin = false;
        this.membersHash = new HashMap();
        this.sprintsHash = new HashMap();
    }

    @action updateSelectedProject(id){
        APICalls.getProject(id).then((project) => {
            if(project.data['error-code']!=null){
                AppState.createErrorMessageGoBack('Error in Project',errorCodes.INVALID_ID,true);
            }
            this.selectedProject = project.data;
            APICalls.getProjectsForUser().then((projects) => {
                let isAdmin=false;
                if(projects !=null) projects.data.map((project) => {
                    if((project.id == this.selectedProject.id))isAdmin = project.isAdmin;
                });
                this.isAdmin = isAdmin;
                this.updateSprints();
                this.updateMembers();
            });
        });
    }

    @action updateSelectedSprint(id){
        APICalls.getSprint(id,this.selectedProject.id).then((sprint) => {
            if(sprint.data['error-code']!=null){
                AppState.createErrorMessageGoBack('Error in Sprint',errorCodes.INVALID_ID,true);
            }
            this.selectedSprint = sprint.data;
        });
    }

    @action updateMembers(){
        APICalls.getAllUsersOfProject(this.selectedProject.id).then((users) => {
            if(users.data['error-code']!=null){
                AppState.createErrorMessage('Error',users.data['error-code']);
            }
            this.members = users.data;
            this.updateMemberHash();
        })
    }

    @action updateSprints(){
        APICalls.getSprintsForProject(this.selectedProject.id).then((sprints) => {
            if(sprints != null && sprints.status == 200){
                this.sprints = sprints.data;
                this.updateBacklogItems();
                this.updateSprintsHash();
            }else{
                this.sprints = null;
            }

        });
    }

   @action updateBacklogItems(){
       APICalls.getAllBacklogItemsForProject(this.selectedProject.id).then((backlogItems) => {
           if(backlogItems != null &&  backlogItems.status == 200){
               this.backlogItems = backlogItems.data;
           }else{
               this.backlogItems = null;
           }
       })
   }

    @action updateMemberHash(){
        this.membersHash = new HashMap();
        this.members.map((member) => {
            this.membersHash.set(member.id,member.username);
        });
    }

    @action updateSprintsHash(){
        this.sprintsHash = new HashMap();
        this.sprints.map((sprint) => {
            this.sprintsHash.set(sprint.id,sprint.name);
        });
    }

    @computed get projectId(){
        return this.selectedProject.id;
    }

    @computed get sprintId(){
        return this.selectedSprint.id;
    }

    @computed get projectName(){
        return this.selectedProject.name;
    }

    @computed get sprintName(){
        return this.selectedSprint != null ? this.selectedSprint.name : '';
    }


}

export default new ProjectState();