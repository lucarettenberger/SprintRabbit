import {observable, action} from "mobx";

class ErrorMessageState {

  @observable error;

  constructor() {
    this.error = null;
  }

  @action closeErrorMessage(){
    this.error = null;
  }

  @action createErrorMessageGoBack(header,body,goBack){
    this.error = {
      header: header,
      body: body,
      goBack: goBack
    }
  }

  @action createErrorMessage(header,body){
    this.createErrorMessageGoBack(header,body,false);
  }


}

export default new ErrorMessageState();