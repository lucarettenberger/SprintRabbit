import {computed, observable, action} from "mobx";
import APICalls from "../ApiCalls/APICalls";
import errorMessageState from './ErrorMessageState';
import projectStore from './ProjectStore';


class UserStore {

    @observable loggedIn;
    @observable user;

    constructor () {
        this.loggedIn = true;
        this.user = null;
        this.updateLoggedIn();
        projectStore.updateProjects();
    }

    @action updateLoggedIn(){
        APICalls.isAuthenticated().then((user) => {
            if(user.authenticated==false){
                this.loggedIn = false;
            }else{
                this.user = user.user;
                this.loggedIn = true;
            }
        });
    }

    @action logOut(){
        APICalls.logOut().then(() => {
            this.updateLoggedIn()
            projectStore.updateProjects();
        });
    }

    @action login(data){
        APICalls.login(data).then((res) => {
            if(res == null || res['error-code'] != null){
                errorMessageState.createErrorMessage('Error',res['error-code']);
            }
            this.updateLoggedIn();
            projectStore.updateProjects();
        });
    }

    @computed get userId(){
        if(this.user == null){
            return -1;
        }
        return this.user['id'];
    }

    @computed get userName(){
        if(this.user == null){
            return '';
        }
        return this.user['username'];
    }

    @action register(data) {
        APICalls.registerUser(data).then(res => {
            window.res = res;
            if(res == null || res['error-code'] != null){
                errorMessageState.createErrorMessage('Error',res['error-code']);
            }
            this.user = res;
            this.loggedIn = true;
        })
    }

}

export default new UserStore();