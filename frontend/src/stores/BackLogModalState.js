import {observable, action} from "mobx";

class BackLogModalState {

    @observable editBacklog;
    @observable backlogItem;

    constructor() {
        this.editBacklog = false;
        this.backlogItem = {
            acceptanceCriteria:null,
            assignee_key:null,
            description:null,
            status:null,
            sprint_key:null,
            title:null
        };
    }

    @action closeEditBacklog(){
        this.editBacklog = false;
        this.backlogItem = {
            acceptanceCriteria:null,
            assignee_key:null,
            description:null,
            status:null,
            sprint_key:null,
            title:null
        };
    }

    @action openEditBacklog(backlogitem){
        this.backlogItem = backlogitem;
        this.editBacklog = true;
    }

}

export default new BackLogModalState();