import axios from "axios";
import errorCodes from '../utils/errorcodes';

const SERVER = 'http://localhost:3000';

class APICalls {

    async registerUser(data) {
        let user  = await axios({
            method: 'post',
            url: SERVER+'/auth/register',
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        window.rest = user;
        return user.data;
    }

    async logOut() {
        await axios({
            method: 'post',
            url: SERVER+'/auth/logout',
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            return err.response;
        });
    }

    async login(data) {
        if(data.username == '' || data.password == ''){
            let res = [];
            res['error-code'] = errorCodes.INVALID_INPUT;
            return res;
        }
        let user  = await axios({
            method: 'post',
            url: SERVER+'/auth/login',
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        window.user = user;
        return user.data;
    }

    async getAllUsers() {
        let users  = await axios({
            method: 'get',
            url: SERVER+'/user',
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return users;
    }

    async getAllUsersOfProject(projectId) {
        let users  = await axios({
            method: 'get',
            url: SERVER+'/project/'+projectId+'/user/',
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return users;
    }

    async getProjectsForUser() {
        let projects  = await axios({
            method: 'get',
            url: SERVER+'/project',
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return projects;
    }

    async getSprintsForProject(id) {
        let sprints  = await axios({
            method: 'get',
            url: SERVER+'/project/'+id+'/sprint',
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return sprints;
    }

    async deleteSprint(sprintId,projectId) {
        let deleted  = await axios({
            method: 'delete',
            url: SERVER+'/project/'+projectId+'/sprint/'+sprintId,
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return deleted;
    }

    async deleteBacklogItem(backlogItemId,projectId) {
        let deleted  = await axios({
            method: 'delete',
            url: SERVER+'/project/'+projectId+'/sprint/0/backlog/'+backlogItemId,
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return deleted;
    }

    async getProject(id) {
        let project  = await axios({
            method: 'get',
            url: SERVER+'/project/'+id,
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return project;
    }

    async getSprint(id,projectId) {
        let sprint  = await axios({
            method: 'get',
            url: SERVER+'/project/'+projectId+'/sprint/'+id,
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return sprint;
    }

    async getAllBacklogItemsForProject(projectId) {
        let project  = await axios({
            method: 'get',
            url: SERVER+'/project/'+projectId+'/backlog',
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err.response;
        });
        return project;
    }

    async isAuthenticated() {
        let user  = await axios({
            method: 'get',
            url: SERVER+'/auth/isloggedin',
            withCredentials: true,
            headers: {
                'content-Type': 'application/json',
                "Accept": "/",
                "Cache-Control": "no-cache"
            }
        }).catch(err => {
            return err;
        });
        if(user.message != null && user.message=='Network Error'){
            return {
                authenticated : false
            };
        }
        window.user = user.data;
       return user.data;
    }

    async createProject(data) {
        let project  = await axios({
            method: 'post',
            url: SERVER+'/project/',
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        window.proj = project;
        return project.data;
    }

    async createSprint(data,projectId) {
        let project  = await axios({
            method: 'post',
            url: SERVER+'/project/'+projectId+'/sprint',
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        window.proj = project;
        return project;
    }

    async createBacklogItem(data,projectId,sprint_key) {
        let path = SERVER+'/project/'+projectId+'/backlog';
        if(sprint_key!=null && sprint_key!=''){
            path = SERVER+'/project/'+projectId+'/sprint/'+sprint_key+'/backlog/';
        }
        let backlogItem  = await axios({
            method: 'post',
            url: path,
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        return backlogItem;
    }

    async updateBacklogItem(data,projectId,sprint_key,backlogId) {
        let path = SERVER+'/project/'+projectId+'/backlog/'+backlogId;
        if(sprint_key!=null && sprint_key!=''){
            path = SERVER+'/project/'+projectId+'/sprint/'+sprint_key+'/backlog/'+backlogId;
        }
        let backlogItem  = await axios({
            method: 'put',
            url: path,
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        return backlogItem;
    }

    async addUserToProject(projectId,userId) {
        let project  = await axios({
            method: 'post',
            url: SERVER+'/project/'+projectId+'/add/'+userId,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        window.proj = project;
        return project;
    }

    async removeUserFromProject(projectId,userId) {
        let project  = await axios({
            method: 'delete',
            url: SERVER+'/project/'+projectId+'/remove/'+userId,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        window.proj = project;
        return project;
    }

    async updateUserProject(data,projectId,userId) {
        let path = SERVER+'/project/'+projectId+'/update/'+userId;
        let backlogItem  = await axios({
            method: 'put',
            url: path,
            data: data,
            credentials: 'same-origin',
            withCredentials:true
        }).catch(err => {
            window.err = err;
            return err.response;
        });
        return backlogItem;
    }
}

const apiCalls = new APICalls();
export default apiCalls;