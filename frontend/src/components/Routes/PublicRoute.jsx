import React, { Component } from "react";
import {Redirect,Route} from "react-router-dom";

const PublicRoute = ({ component: Component, isAuthenticated : authenticated, ...rest }) => (
    <Route {...rest} render={(props) => (
        authenticated === false
            ? <Component {...props} />
            : <Redirect to='/' />
    )} />
);

export default PublicRoute;