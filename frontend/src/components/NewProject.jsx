import React from "react";
import { Button, Divider, Form, Segment} from 'semantic-ui-react'
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";

@inject("projectStore")
@withRouter
@observer
class NewProject extends React.Component {

    inputs = {name: ''};
    handleChange = (e, { name, value }) => (this.inputs[name] = value);

    handleNewProjectClick = () => {
        this.props.projectStore.createProject(this.inputs);
        this.props.history.push('/');
    };

    render() {
        return (
            <Segment>
                <Form onSubmit={this.handleNewProjectClick}>
                    <Form.Input label='Name' type='name' name='name' onChange={this.handleChange}/>
                    <Button type='submit' fluid>Create Project</Button>
                </Form>
            </Segment>
        );
    }
}

export default NewProject;