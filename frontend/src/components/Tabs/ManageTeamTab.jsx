import React, { Component } from "react";
import {Table, Grid, Form, Header, Button, Icon} from 'semantic-ui-react'
import { inject, observer } from "mobx-react";
import {NavLink, Link, withRouter} from "react-router-dom";
import EditUser from "../Modals/EditUser";


@inject('projectState')
@inject('projectStore')
@inject('userStore')
@inject('manageTeamState')
@withRouter
@observer
class ManageTeamTab extends React.Component {

    constructor(props){
        super(props);
        props.projectStore.getAllUsers();
        this.state ={
            usersToAdd: null,
            usersToRemove: null
        }
    }

    addInput = {user: null};
    handleChangeAdd = (e, { name, value }) =>{
        this.addInput[name] = value;
    };

    addUser = () => {
        if(this.addInput['user'] != null){
            this.props.projectStore.addUserToProject(this.props.projectState.projectId,this.addInput['user']);
        }

    };

    removeInput = {user: null};
    handleChangeRemove = (e, { name, value }) =>{
        this.removeInput[name] = value;
    };

    removeUser = () => {
        if(this.removeInput['user'] != null){
            if(this.removeInput['user'] == this.props.userStore.userId)return;
            this.props.projectStore.removeUserFromProject(this.props.projectState.projectId,this.removeInput['user']);
        }

    };

    getUsersToAdd = () => {
        let users = this.props.projectStore.users;
        let members = this.props.projectState.members;
        if(users==null || users.length == 0)return [];
        let returnMembers= [];
        users.map((user) => {
            let alreadyMember = false;
            members.map((member) => {
               if(member.id == user.id){
                   alreadyMember=true;
               }
            });
            if(!alreadyMember)
            returnMembers.push({
                value: user.id,
                text: user.username
            });
        });
        returnMembers.push({
            value: null,
            text: 'No User'
        });
        return returnMembers;
    };

    getUsersToRemove = () => {
        let members = this.props.projectState.members;
        let returnMembers= [];
        members.map((user) => {
            returnMembers.push({
                value: user.id,
                text: user.username
            });
        });
        returnMembers.push({
            value: null,
            text: 'No User'
        });
        return returnMembers;
    };

    getUsersInTable = () => {
        let userTable= [];
        this.props.projectState.members.map( user => {
            userTable.push(
                <Table.Row>
                    <Table.Cell>{user.username}</Table.Cell>
                    <Table.Cell>{user.role}</Table.Cell>
                    <Table.Cell>{user.isAdmin ? 'Yes' : 'No'}</Table.Cell>
                    <Table.Cell textAlign='center'>
                        <Button size='tiny' onClick={this.handleEditClick.bind(this, user.id)}><Icon name='edit'/> Edit User</Button>
                    </Table.Cell>
                </Table.Row>);
        });
        return userTable;
    };

    handleEditClick = (id) => {
        let members = this.props.projectState.members;
        members.map((member) => {
            if(member.id == id){
                this.props.manageTeamState.openEditUser({
                    id: member.id,
                    role: member.role,
                    isAdmin: member.isAdmin,
                    username: member.username
                });
            }
        });
    };

    render() {
        return (
            <div>
            <EditUser open={false}/>
            <Header as='h2'>Add/Remove Users</Header>
            <Grid>
                <Grid.Column width={8}>
                    <Form>
                        <Form.Dropdown label='Add User To Project' defaultValue={0} placeholder='Select User' name='user' fluid selection
                                       options={this.getUsersToAdd()} onChange={this.handleChangeAdd} />
                        <Button onClick={this.addUser} >Add User</Button>
                    </Form>
                </Grid.Column>
                <Grid.Column width={8}>
                    <Form>
                        <Form.Dropdown label='Remove User From Project' defaultValue={0} placeholder='Select User' name='user' fluid selection
                                       options={this.getUsersToRemove()} onChange={this.handleChangeRemove} />
                        <Button onClick={this.removeUser} >Remove User</Button>
                    </Form>
                </Grid.Column>
            </Grid>
            <Header as='h2'>Edit Users</Header>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name:</Table.HeaderCell>
                        <Table.HeaderCell>Role:</Table.HeaderCell>
                        <Table.HeaderCell>Admin:</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.getUsersInTable()}
                </Table.Body>
            </Table>

            </div>
        );
    }
}

export default ManageTeamTab;