import React, { Component } from "react";
import { Button, Table, Icon} from 'semantic-ui-react'
import { inject, observer } from "mobx-react";
import {Link, withRouter} from "react-router-dom";
import backlogStatus from '../../utils/backlogStatus';
import EditBacklogItem from "../Modals/EditBacklogItem";

@withRouter
@inject('projectState')
@inject('projectStore')
@inject('backlogState')
@observer
class ProductBacklogTab extends React.Component {

    constructor(props){
        super(props);

    }

    getMatchUrl = () =>{
        let url = this.props.match.url;
        window.url = url;
        if(url.substring(url.length-1)=='/'){
            return url.substring(0,url.length-1)
        }
        return url;
    };

    handleDeleteClick = (id) =>{
        this.props.projectStore.deleteBacklogItem(id,
            this.props.projectState.selectedProject.id);
    };

    handleEditClick = (id) => {
        let backlogItems = this.props.projectState.backlogItems;
        backlogItems.map((backlogItem) => {
           if(backlogItem.id == id){
               this.props.backlogState.openEditBacklog(backlogItem);
               return;
           }
        });
    };

    getSprintsInTable = (backlogs,isAdmin) => {
        let backlogTable= [];
        if(backlogs == null || backlogs.length==0)return;
        backlogs.map( backlog => {
            backlogTable.push(
                <Table.Row>
                    <Table.Cell>{backlog.title}</Table.Cell>
                    <Table.Cell>{backlog.sprint_key == null ? 'No Associated Sprint' :
                        this.props.projectState.sprintsHash.get(backlog.sprint_key)}</Table.Cell>
                    <Table.Cell>{backlogStatus(backlog.status)}</Table.Cell>
                    {isAdmin ? <Table.Cell textAlign='right'>
                        <Button size='tiny' onClick={this.handleEditClick.bind(this, backlog.id)}>
                            <Icon name='edit'/> Edit BacklogItem</Button>
                    </Table.Cell> : ''}
                    {isAdmin ? <Table.Cell >
                        <Button size='tiny' color='red' onClick={this.handleDeleteClick.bind(this, backlog.id)}>
                            <Icon name='delete'/> Delete BacklogItem</Button>
                    </Table.Cell> : ''}
                </Table.Row>);
        });
        return backlogTable;
    };

    render() {
        let {isAdmin,backlogItems} = this.props.projectState;
        return (
            <div>
                <EditBacklogItem open={false}/>
                <Table>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Name:</Table.HeaderCell>
                            <Table.HeaderCell>Sprint:</Table.HeaderCell>
                            <Table.HeaderCell>Status:</Table.HeaderCell>
                            {isAdmin ? <Table.HeaderCell></Table.HeaderCell> : ''}
                            {isAdmin ? <Table.HeaderCell></Table.HeaderCell> : ''}
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.getSprintsInTable(backlogItems,isAdmin)}
                    </Table.Body>
                </Table>
                {isAdmin ? <Link to={this.getMatchUrl()+'/newBacklogItem'}><Button fluid><Icon name='add' /> Add new Backlog Item</Button></Link> : ''}
            </div>
        );
    }
}

export default ProductBacklogTab;