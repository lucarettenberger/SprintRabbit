import React, { Component } from "react";
import {Segment, Breadcrumb, Image, Grid, Button, Header, Table, Icon} from 'semantic-ui-react'
import { inject, observer } from "mobx-react";
import {NavLink, Link, withRouter} from "react-router-dom";
import APICalls from '../../ApiCalls/APICalls';
import Moment from "react-moment";
import errorCodes from '../../utils/errorcodes';

@withRouter
@inject('appState')
@inject("projectState")
@inject('projectStore')
@observer
class SprintsTab extends React.Component {

    constructor(props){
        super(props);
        props.projectState.updateSprints();
    }

    getMatchUrl = () =>{
        let url = this.props.match.url;
        window.url = url;
        if(url.substring(url.length-1)=='/'){
            return url.substring(0,url.length-1)
        }
        return url;
    };

    handleDeleteClick = (id) =>{
        this.props.projectStore.deleteSprint(id,this.props.projectState.selectedProject.id);
    };

    getSprintsInTable = (sprints,isAdmin) => {
        let projectsTable= [];
        window.spr = sprints;
        if(sprints == null || sprints.length==0)return;
        sprints.map( sprint => {
            projectsTable.push(
                <Table.Row>
                    <Table.Cell><Link to={this.getMatchUrl()+'/'+sprint.id}>{sprint.name}</Link></Table.Cell>
                    <Table.Cell><Moment format="DD.MM.YYYY">{sprint.startTime}</Moment></Table.Cell>
                    <Table.Cell><Moment format="DD.MM.YYYY">{sprint.endTime}</Moment></Table.Cell>
                    {isAdmin ? <Table.Cell textAlign='right'>
                                    <Button size='tiny' color='red' onClick={this.handleDeleteClick.bind(this, sprint.id)}><Icon name='delete'/> Delete Sprint</Button>
                              </Table.Cell> : ''}
                </Table.Row>);
        });
        return projectsTable;
    };

    render() {
        let {isAdmin,sprints} = this.props.projectState;
        return (
            <div>
                    <Table >
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Name:</Table.HeaderCell>
                                <Table.HeaderCell>Start:</Table.HeaderCell>
                                <Table.HeaderCell>End:</Table.HeaderCell>
                                {isAdmin ? <Table.HeaderCell></Table.HeaderCell> : ''}
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {this.getSprintsInTable(sprints,isAdmin)}
                        </Table.Body>
                    </Table>
                {isAdmin ? <Link to={this.getMatchUrl()+'/newSprint'}><Button fluid><Icon name='add' /> Add new Sprint</Button></Link> : ''}
            </div>
        );
    }
}

export default SprintsTab;