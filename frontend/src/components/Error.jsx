import React, { Component } from 'react'
import { Button, Confirm, Modal, Header, Icon } from 'semantic-ui-react'
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";
import {errorText} from './../utils/errorTexts';


@inject("appState")
@withRouter
@observer
class Error extends Component {

    close = () => {
        if(this.props.appState.error.goBack!= null && this.props.appState.error.goBack==true){
            this.props.history.goBack();
        }
        this.props.appState.closeErrorMessage();
    }

    render() {
        let {error} = this.props.appState;
        return (
            <Modal open={error != null} basic size='small'>
                <Header icon='exclamation' content={error != null ? error.header : ''} />
                <Modal.Content>
                    <p>
                        {error != null ? errorText(error.body) : ''}
                    </p>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='red' inverted onClick={this.close}>Okay</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}
export default Error;
