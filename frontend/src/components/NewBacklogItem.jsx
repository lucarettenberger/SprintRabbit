import React from "react";
import {Button, Form, Header, Segment} from 'semantic-ui-react'
import {withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";

@inject("projectStore")
@inject("projectState")
@withRouter
@observer
class NewBacklogItem extends React.Component {

    inputs = {title: '', description: '', acceptanceCriteria: '',sprint_key:''};
    handleChange = (e, { name, value }) =>{
        this.inputs[name] = value;
    };

    handleNewBacklogItemClick = () => {
        this.props.projectStore.createBacklogItem(this.inputs,this.props.projectState.selectedProject.id);
        this.props.history.goBack();
    };

    getSprints = () => {
        let sprints = this.props.projectState.sprints;
        if(sprints==null)return [];
        let returnSprints= [];
        this.props.projectState.sprints.map((sprint) => {
            returnSprints.push({
                value: sprint.id,
                text: sprint.name
            });
        });
        returnSprints.push({
            value: null,
            text: 'No Sprint'
        });
        return returnSprints;
    };

    render() {
        return (
            <Segment>
                <Header as='h2'>Add new Backlog Item to Project</Header>
                <Form onSubmit={this.handleNewBacklogItemClick}>
                    <Form.Input label='Title' type='title' name='title' onChange={this.handleChange}/>
                    <Form.TextArea label='Description' name='description' placeholder='Backlog Item Description'
                                   onChange={this.handleChange} />
                    <Form.TextArea label='Acceptance Criteria' name='acceptanceCriteria' placeholder='Acceptance Criteria'
                                   onChange={this.handleChange}/>
                    <Form.Dropdown placeholder='Select Sprint' name='sprint_key' fluid selection
                                   options={this.getSprints()} onChange={this.handleChange}></Form.Dropdown>
                    <Button type='submit' fluid>Create Backlog Item</Button>
                </Form>
            </Segment>
        );
    }
}

export default NewBacklogItem;