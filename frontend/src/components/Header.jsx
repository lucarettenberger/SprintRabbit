import React, { Component } from "react";
import { Segment , Breadcrumb,Image, Grid,Button} from 'semantic-ui-react'
import { inject, observer } from "mobx-react";
import {NavLink, Link, withRouter} from "react-router-dom";


@inject("userStore")
@inject("projectState")
@withRouter
@observer
class Header extends React.Component {

    getSections = (location) => {
        let urlUntilNow = '/';
        let sections= [];
        let projectName = this.props.projectState.projectName;
        let sprintName = this.props.projectState.sprintName;
        let locationsSplit = location.substring(1).split('/');
        sections.push(<Breadcrumb.Section><NavLink to={urlUntilNow}>Home</NavLink></Breadcrumb.Section>);
        if(locationsSplit.length > 0 && locationsSplit[0]!='' && locationsSplit[0].toLowerCase()!='newproject'){
            sections.push(<Breadcrumb.Divider icon='right chevron' />);
            sections.push(<Breadcrumb.Section><NavLink to={'/'+locationsSplit[0]}>{projectName || locationsSplit[0]}</NavLink></Breadcrumb.Section>);
        }
        if(locationsSplit.length > 1 && locationsSplit[1]!='' && locationsSplit[0]!=''){
            sections.push(<Breadcrumb.Divider icon='right chevron' />);
            sections.push(<Breadcrumb.Section><NavLink to={locationsSplit[1]}>{sprintName || locationsSplit[1]}</NavLink></Breadcrumb.Section>);
        }
        return sections;
    };

    getUserNameCut(userName){
        return userName.length > 16 ? userName.substring(0,16)+'...' : userName
    }

    render() {
        let {loggedIn, userName} = this.props.userStore;
        let {history} = this.props;
        let sections= this.getSections(this.props.location.pathname);
        return (
            <React.Fragment>
                <Link to='/'><Image src='/src/images/logo.png' size='small' /></Link>
                <Grid columns={2}>
                    <Grid.Row stretched>
                    <Grid.Column width={12}>
                <Segment>
                    <Breadcrumb>
                        {sections}
                    </Breadcrumb>
                </Segment>
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Button onClick={() => (loggedIn ? this.props.userStore.logOut() : history.push('/Register'))}>
                            {loggedIn ? ('Logout '+this.getUserNameCut(userName)) : 'Register/Login'}</Button>
                    </Grid.Column>
                    </Grid.Row>
                </Grid>
            </React.Fragment>
        );
    }
}

export default Header;