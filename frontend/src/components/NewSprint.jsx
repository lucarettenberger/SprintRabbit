import React, { Component } from "react";
import {Segment,Button, Header, Table, Icon,Form} from 'semantic-ui-react'
import { inject, observer } from "mobx-react";
import {withRouter} from "react-router-dom";
import {DateInput} from "semantic-ui-calendar-react";

@withRouter
@inject('projectState')
@inject('projectStore')
@observer
class NewSprint extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            startTime: '',
            endTime: ''
        };
    }

    inputs = {name: '',startTime : '',endTime: ''};
    handleChange = (e, { name, value }) => {
        this.inputs[name] = value;
        this.setState({
            startTime: this.inputs['startTime'],
            endTime: this.inputs['endTime']
        });
    };

    handleNewSprintClick = () => {
        this.props.projectStore.createSprint(this.inputs,this.props.projectState.projectId);
        this.props.history.goBack();
    };

    render() {
        let {selectedProject} = this.props.projectState;
        let today = new Date();
        return (
            <Segment>
                <Header as='h2'>Add new Sprint to Project</Header>
                <Form  disabled={true} onSubmit={this.handleNewSprintClick}>
                    <Form.Input label='Name:' type='name' name='name' onChange={this.handleChange}/>
                    <Form.Input label='Start Time:' type='startTime' name='startTime'><DateInput
                        name="startTime"
                        placeholder="Date"
                        dateFormat={'MM-DD-YYYY'}
                        minDate={today}
                        value={this.state.startTime}
                        iconPosition="left"
                        onChange={this.handleChange}
                    /></Form.Input>
                    <Form.Input label='End Time:' type='endTime' name='endTime'><DateInput
                        name="endTime"
                        placeholder="Date"
                        dateFormat={'MM-DD-YYYY'}
                        minDate={today}
                        value={this.state.endTime}
                        iconPosition="left"
                        onChange={this.handleChange}
                    /></Form.Input>
                    <Button type='submit' disabled={this.inputs.startTime == '' ||
                                                    this.inputs.endTime == ''} fluid>Add Sprint</Button>
                </Form>
            </Segment>
        );
    }
}

export default NewSprint;