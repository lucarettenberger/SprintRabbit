import React, { Component } from 'react'
import {Button, Confirm, Modal, Header, Icon, Segment,Form} from 'semantic-ui-react'
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";

@inject('manageTeamState')
@inject('projectStore')
@inject('projectState')
@inject('userStore')
@withRouter
@observer
class EditBacklogItem extends Component {

    constructor(props){
        super(props);
    }

    inputs = {role:null, isAdmin:null};
    handleChange = (e, { name, value }) =>{
        this.inputs[name] = value;
    };

    close = () => {
        this.props.projectStore.updateUserProject(this.inputs,
                                           this.props.projectState.projectId,
                                           this.props.manageTeamState.user.id);
        this.props.manageTeamState.closeEditUser();
    };

    closeWithoutSave = () => this.props.manageTeamState.closeEditUser();

    render() {
        let {role,isAdmin,username,id} = this.props.manageTeamState.user;
        this.inputs = {
            role:role,
            isAdmin:isAdmin
        };
        return (
            <Modal open={this.props.manageTeamState.editUser} basic size='small'>
                <Modal.Content>
                    <Segment>
                        <Header as='h2'>Edit User {username}</Header>
                        <Form>
                            <Form.Input  defaultValue={role} label='Role' type='role' name='role' onChange={this.handleChange}/>
                            <Form.Dropdown disabled={id == this.props.userStore.userId} label='Is Admin' defaultValue={isAdmin ? 1 : 0}  name='isAdmin' fluid selection
                                           options={[{value: 0, text: 'No'},{value: 1, text: 'Yes'}]} onChange={this.handleChange} />
                        </Form>
                    </Segment>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='green' onClick={this.close} inverted >Save</Button>
                    <Button basic color='red' onClick={this.closeWithoutSave} inverted >Cancel</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}
export default EditBacklogItem;