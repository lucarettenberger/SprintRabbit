import React, { Component } from 'react'
import {Button, Modal, Header, Segment,Form} from 'semantic-ui-react'
import {inject, observer} from "mobx-react";
import {withRouter} from "react-router-dom";


@inject('backlogState')
@inject('projectState')
@inject('projectStore')
@withRouter
@observer
class EditBacklogItem extends Component {

    constructor(props){
        super(props);
    }

    inputs = {title: null, description: null, acceptanceCriteria: null,sprint_key:null,assignee_key:null,status: null};
    handleChange = (e, { name, value }) =>{
        this.inputs[name] = value;
    };

    close = () => {
        this.props.projectStore.updateBacklogItem(this.inputs,this.props.projectState.projectId,
            this.inputs.sprint_key,this.props.backlogState.backlogItem.id);
        this.props.backlogState.closeEditBacklog();
    };

    closeWithoutSave = () => this.props.backlogState.closeEditBacklog();

    getSprints = () => {
        let sprints = this.props.projectState.sprints;
        if(sprints==null)return [];
        let returnSprints= [];
        this.props.projectState.sprints.map((sprint) => {
            returnSprints.push({
                value: sprint.id,
                text: sprint.name
            });
        });
        returnSprints.push({
            value: null,
            text: 'No Sprint'
        });
        return returnSprints;
    };

    getAsignees = () => {
        let members = this.props.projectState.members;
        if(members==null)return [];
        let returnMembers= [];
        members.map((member) => {
            returnMembers.push({
                value: member.id,
                text: member.username
            });
        });
        returnMembers.push({
            value: null,
            text: 'No User'
        });
        return returnMembers;
    };

    render() {
        let {acceptanceCriteria,assignee_key,description,sprint_key,title} = this.props.backlogState.backlogItem;
        this.inputs = {
            title: title,
            description: description,
            acceptanceCriteria: acceptanceCriteria,
            sprint_key:sprint_key,
            assignee_key:assignee_key
        }
        return (
            <Modal open={this.props.backlogState.editBacklog} basic size='small'>
                <Modal.Content>
                    <Segment>
                        <Header as='h2'>Edit Backlog Item</Header>
                        <Form>
                            <Form.Input  defaultValue={title} label='Title' type='title' name='title' onChange={this.handleChange}/>
                            <Form.TextArea defaultValue={description}  label='Description' name='description' placeholder='Backlog Item Description'
                                           onChange={this.handleChange} />
                            <Form.TextArea defaultValue={acceptanceCriteria} label='Acceptance Criteria' name='acceptanceCriteria' placeholder='Acceptance Criteria'
                                           onChange={this.handleChange}/>
                            <Form.Dropdown label='Sprint' defaultValue={sprint_key} placeholder='Select Sprint' name='sprint_key' fluid selection
                                           options={this.getSprints()} onChange={this.handleChange} />
                            <Form.Dropdown label='Asignee' defaultValue={assignee_key} placeholder='Select Sprint' name='assignee_key' fluid selection
                                           options={this.getAsignees()} onChange={this.handleChange} />
                        </Form>
                    </Segment>
                </Modal.Content>
                <Modal.Actions>
                    <Button basic color='green' onClick={this.close} inverted >Save</Button>
                    <Button basic color='red' onClick={this.closeWithoutSave} inverted >Cancel</Button>
                </Modal.Actions>
            </Modal>
        )
    }
}
export default EditBacklogItem;