import React, { Component } from "react";
import StartPage from './../pages/StartPage'
import PrivateRoute from '../components/Routes/PrivateRoute';
import Register from "./Register";
import { inject, observer } from "mobx-react";
import {withRouter,Switch} from "react-router-dom";
import Error from "../components/Error";
import Login from "./Login";
import PublicRoute from "../components/Routes/PublicRoute";
import NewProject from "../components/NewProject";
import Project from "./Project";

@inject("userStore")
@withRouter
@observer
class App extends React.Component {

    render() {

        let {loggedIn} = this.props.userStore;
        return (
            <div>
                <Error/>
                <PrivateRoute exact path="/" isAuthenticated={loggedIn} component={StartPage} />
                <Switch>
                    <PrivateRoute exact path="/newProject" isAuthenticated={loggedIn} component={NewProject} />
                    <PublicRoute exact path="/register" isAuthenticated={loggedIn} component={Register} />
                    <PublicRoute exact path="/login" isAuthenticated={loggedIn} component={Login} />
                    <PrivateRoute  path="/:projectId" isAuthenticated={loggedIn} component={Project} />
                </Switch>
            </div>
        );
    }
}

export default App;
