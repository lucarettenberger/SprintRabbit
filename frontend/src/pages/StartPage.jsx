import React from "react";
import { Header, Table, Button, Icon ,Breadcrumb} from 'semantic-ui-react'
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import Moment from "react-moment";

@inject("userStore")
@inject("projectStore")
@withRouter
@observer
class StartPage extends React.Component {

    getProjectsInTable = (projects) => {
        let projectsTable= [];
        if(projects == null || projects.length==0)return;
        projects.map( project => {
            projectsTable.push(
                <Table.Row>
                    <Table.Cell><Link to={'/'+project.id}>{project.name}</Link></Table.Cell>
                    <Table.Cell>{project.role}</Table.Cell>
                    <Table.Cell>{project.isAdmin ? <Icon name='check'/> : <Icon name='ban'/> }</Table.Cell>
                    <Table.Cell><Moment format="DD.MM.YYYY">{project.createdAt}</Moment></Table.Cell>
                </Table.Row>);
        });
        return projectsTable;
    };

    render() {
        let {userName} = this.props.userStore;
        let {projects} = this.props.projectStore;
        return (
            <div>
                <Header as='h1'>Hey, {userName}!</Header>
                <Header as='h2'>Your current Projects:</Header>
                <Table >
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Name:</Table.HeaderCell>
                            <Table.HeaderCell>Role:</Table.HeaderCell>
                            <Table.HeaderCell>Are You Admin?</Table.HeaderCell>
                            <Table.HeaderCell>Created</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {this.getProjectsInTable(projects)}
                    </Table.Body>
                </Table>
                <Link to={'/newProject'}><Button fluid><Icon name='add' /> Create a new Project</Button></Link>
            </div>
        );
    }
}

export default StartPage;