import React from "react";
import { Header,Tab} from 'semantic-ui-react'
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";
import ManageTeamTab from "../components/Tabs/ManageTeamTab";
import {Route,Switch} from "react-router-dom";
import SprintsTab from "../components/Tabs/SprintsTab";
import Sprint from "./Sprint";
import ProductBacklogTab from "../components/Tabs/ProductBacklogTab";
import NewSprint from "../components/NewSprint";
import NewBacklogItem from "../components/NewBacklogItem";

@inject("projectState")
@withRouter
@observer
class Project extends React.Component {

    constructor(props) {
        super(props);
        props.projectState.updateSelectedProject(props.match.params.projectId);
        this.state = {
          selectedTab:1
        };
    }

    getPanes = () =>{
        let panes = [
            { menuItem: 'SprintsTab', render: () => <Tab.Pane><SprintsTab/></Tab.Pane> },
            { menuItem: 'Product Backlog', render: () => <Tab.Pane><ProductBacklogTab/></Tab.Pane> }
        ];
        if(this.props.projectState.isAdmin)panes.push({menuItem: 'Manage Team', render: () => <Tab.Pane><ManageTeamTab/></Tab.Pane>});
        return panes;
    };

    render() {
        let {projectId,projectName} =  this.props.projectState;
        return (
            <div>
                <Header as='h1'>Project: <Link to={'/'+projectId}>{projectName}</Link></Header>
                <Switch>
                    <Route path={this.props.match.url+'/newsprint'}  component={NewSprint}/>
                    <Route path={this.props.match.url+'/newbacklogitem'} component={NewBacklogItem}/>
                    <Route path={this.props.match.url+'/:sprintId'} component={Sprint}/>
                    <Tab panes={this.getPanes()} />
                </Switch>
            </div>
        );
    }
}

export default Project;