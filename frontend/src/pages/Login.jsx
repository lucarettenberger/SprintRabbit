import React from "react";
import { Button, Divider, Form, Segment} from 'semantic-ui-react'
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";

@inject("userStore")
@withRouter
@observer
class Register extends React.Component {

    inputs = {password: '' ,username: ''};
    handleChange = (e, { name, value }) => (this.inputs[name] = value);

    handleLoginClick = () => {
        this.props.userStore.login(this.inputs);
    };

    render() {
        return (
            <Segment>
                <Form onSubmit={this.handleLoginClick}>
                    <Form.Input label='Username' type='username' name='username' onChange={this.handleChange}/>
                    <Form.Input label='Password' type='password' name='password' onChange={this.handleChange}/>
                    <Button type='submit' fluid>Login</Button>
                </Form>
                <Divider horizontal>Or</Divider>
                <Link to='/Register'><Button fluid>Register</Button></Link>

            </Segment>
        );
    }
}

export default Register;