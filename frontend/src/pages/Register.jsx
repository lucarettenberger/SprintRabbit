import React from "react";
import { Button, Divider, Form, Segment} from 'semantic-ui-react'
import {Link, withRouter} from "react-router-dom";
import {inject, observer} from "mobx-react";

@inject("userStore")
@withRouter
@observer
class Register extends React.Component {

    inputs = { firstName: '',lastName: '', email: '',password: '' ,username: ''};
    handleChange = (e, { name, value }) => (this.inputs[name] = value);

    handleRegisterClick = () => {
        this.props.userStore.register(this.inputs);
    };

    render() {
        return (
            <Segment>
                <Form onSubmit={this.handleRegisterClick}>
                    <Form.Input label='Username' type='username' name='username' onChange={this.handleChange}/>
                    <Form.Input label='First Name' type='name' name='firstName' onChange={this.handleChange}/>
                    <Form.Input label='Last Name' type='name' name='lastName' onChange={this.handleChange}/>
                    <Form.Input label='Email' type='email' name='email' onChange={this.handleChange}/>
                    <Form.Input label='Password' type='password' name='password' onChange={this.handleChange}/>
                    <Button type='submit' fluid>Register</Button>
                </Form>
                <Divider horizontal>Or</Divider>
                <Link to='/Login'><Button fluid>Login</Button></Link>

            </Segment>
        );
    }
}

export default Register;