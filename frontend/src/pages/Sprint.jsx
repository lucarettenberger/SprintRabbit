import React, { Component } from "react";
import {Segment, Breadcrumb, Image, Grid, Button, Header} from 'semantic-ui-react'
import { inject, observer } from "mobx-react";
import {NavLink, Link, withRouter} from "react-router-dom";
import Board from "react-trello";

@inject("route")
@inject("projectState")
@inject('projectStore')
@withRouter
@observer
class Sprint extends React.Component {

    fillBoard = () => {
        let width = 270;
        let height = 800;
        let skeleton = {
            lanes: [
                {laneDraggable:false, id: 1, title: 'Commited', cards: [], "style": {"height": height, "width": width}},
                {id: 2,title: 'In Progress',cards: [],"style": {"height": height, "width": width}},
                {id: 3,title: 'Done',cards: [],"style": {"height": height,"width": width}},
                {id: 4,title: 'Approved',label: 'Admin Only',droppable: false,cards: [], "style": {"height": height,"width": width}}
            ],
        };
        if(this.props.projectState.isAdmin)skeleton['lanes'][3].droppable = true;
        window.skel = skeleton;
        let backlogs = this.props.projectState.backlogItems;
        if(backlogs!=null){
            backlogs.map((backlog) => {
                if(backlog.sprint_key==this.props.match.params.sprintId){
                    let card = {id: backlog.id, title: backlog.title, description: backlog.description,
                        label: this.props.projectState.membersHash.get(backlog.assignee_key)};
                    if(backlog.status!=0) skeleton['lanes'][backlog.status-1].cards.push(card);
                }
            });
        }
        return skeleton;
    };

    cardDragged = (cardId, sourceLaneId, targetLaneId, position, cardDetails) => {
            this.props.projectStore.updateBacklogItem({
                status: targetLaneId
            },this.props.projectState.projectId, this.props.projectState.sprintId,cardId);
            return false;
    };

    isSprintFinished() {
        let backlogs = this.props.projectState.backlogItems;
        let finished = true;
        if (backlogs != null) {
            backlogs.map((backlog) => {
                if(backlog.sprint_key==this.props.match.params.sprintId){
                    if(backlog.status < 4){
                        finished=false;
                    }
                }
            });
        }
        return finished;
    }


    render() {
        this.props.projectState.updateSelectedSprint(this.props.match.params.sprintId);
        return (
            <div>
                <Header as='h2'>Sprint {this.props.projectState.sprintName}</Header>
            <Board handleDragEnd={this.cardDragged}
                   data={this.fillBoard()}
                   cardDraggable={!this.isSprintFinished()}
                   draggable
                   laneDraggable={false}
                   style={{background:'white'}}
                   />
            </div>
        );
    }
}

export default Sprint;