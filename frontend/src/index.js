import React from "react";
import ReactDOM from "react-dom";
import App from "./pages/App";

import 'semantic-ui-css/semantic.min.css';
import { Router, Route} from 'react-router-dom';
import {RouterStore, syncHistoryWithStore} from 'mobx-react-router';
import { BrowserRouter } from 'react-router-dom'
import { Container, Divider} from 'semantic-ui-react'

import AppState from "./stores/ErrorMessageState";
import UserStore from './stores/UserStore';
import ProjectStore from './stores/ProjectStore'
import {Provider} from "mobx-react";

import createBrowserHistory from 'history/createBrowserHistory';
import Header from "./components/Header";
import ProjectState from "./stores/ProjectState";
import BackLogState from "./stores/BackLogModalState";
import ManageTeamState from "./stores/ManageTeamModalState";

//Browser history and routing store lib: https://github.com/alisd23/mobx-react-router
const browserHistory = createBrowserHistory();
const routingStore = new RouterStore();

const stores = {
    route: routingStore,
    appState: AppState,
    userStore: UserStore,
    projectStore: ProjectStore,
    projectState: ProjectState,
    backlogState : BackLogState,
    manageTeamState: ManageTeamState
};

const history = syncHistoryWithStore(browserHistory, routingStore);

ReactDOM.render(
    <Provider {...stores}>
            <BrowserRouter history={history}>
                <Container>
                    <Header/>
                    <Divider/>
                    <App/>
                </Container>
            </BrowserRouter>
        </Provider>
    ,document.getElementById('root'));