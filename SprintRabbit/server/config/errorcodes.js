/**
 * Project-wide constants for error-codes
 */

module.exports = Object.freeze({ //Freeze 'freezes' variables to be truly constant
    UNKNOWN_ERROR:             -1,
    USERNAME_ALREADY_TAKEN:     1,
    COULD_NOT_CREATE_ACCOUNT:   2,
    INVALID_INPUT:              3,
    USERNAME_DOES_NOT_EXIST:    4,
    WRONG_PASSWORD:             5,
    NOT_LOGGED_IN:              6,
    INVALID_ID:                 7
});
