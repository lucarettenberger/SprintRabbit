var express = require('express');
var router = express.Router();
var auth = require('../actions/auth');
var sprintActions = require('../actions/sprint');


/**
 * Get all sprints for project
 */
router.get('/', auth.isLoggedIn,auth.isMemberOfProject, function(req, res, next) {
    sprintActions.getAllSprints(req,res,req.body.projectId);
});


/**
 * get certain sprint for project
 */
router.get('/:sprintId', auth.isLoggedIn, auth.isMemberOfProject, function(req, res, next) {
    sprintActions.getSprint(req,res,req.params.sprintId);
});

/**
 * create new sprint for project
 */
router.post('/', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    sprintActions.createSprint(req,res,req.body.projectId);
});

/**
 * update sprint for project
 */
router.put('/:sprintId', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    sprintActions.updateSprint(req,res,req.params.sprintId);
});


/**
 * delete sprint for project
 */
router.delete('/:sprintId', auth.isLoggedIn,auth.isAdminOfProject, function(req, res, next) {
    sprintActions.deleteSprint(req,res,req.params.sprintId);
});

module.exports = router;