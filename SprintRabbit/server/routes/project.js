var express = require('express');
var router = express.Router();
var projectActions = require('../actions/project');
var backlogActions = require('../actions/backlogitem');
var auth = require('../actions/auth');


/**
 * Get all Projects
 */
router.get('/', auth.isLoggedIn,  function(req, res, next) {
    projectActions.getAllProjects(req,res,req.user.id);
});

/**
 * Get project by id
 */
router.get('/:projectId', auth.isLoggedIn,auth.isMemberOfProject, function(req, res, next) {
    projectActions.getProjectById(req,res,req.params.projectId);
});

/**
 * Get get all users of project
 */
router.get('/:projectId/user/', auth.isLoggedIn, auth.isMemberOfProject, function(req, res, next) {
    projectActions.getAllUsers(req,res,req.params.projectId);
});

/**
 * Create new Project
 */
router.post('/', auth.isLoggedIn, function(req, res) {
    projectActions.createProject(req,res);
});

/**
 * Delete Project by id (only possible if logged in user is admin of the project )
 */
router.delete('/:projectId', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    projectActions.deleteProject(req,res,req.params.projectId);
});

/**
 * Add user to the project. (only possible if logged in user is admin of the project )
 */
router.post('/:projectId/add/:userId', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    projectActions.addUserToProject(req,res,req.params.userId,req.params.projectId);
});

/**
 * remove user from the project. (only possible if logged in user is admin of the project )
 */
router.delete('/:projectId/remove/:userId', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    projectActions.removeUserFromProject(req,res,req.params.userId,req.params.projectId);
});

/**
 * update userProject relation. (only possible if logged in user is admin of the project)
 */
router.put('/:projectId/update/:userId', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    projectActions.updateUserProject(req,res,req.params.userId,req.params.projectId);
});

/**
 * Get get all backlog items of project
 */
router.get('/:projectId/backlog/', auth.isLoggedIn,auth.isMemberOfProject, function(req, res, next) {
    backlogActions.getAllBacklogItemsForProject(req,res,req.params.projectId);
});

/**
 * create new backlog item
 */
router.post('/:projectId/backlog/', auth.isLoggedIn, auth.isMemberOfProject, function(req, res, next) {
    backlogActions.createBacklogItem(req,res,req.params.projectId,undefined);
});

/**
 * update backlog item
 */
router.put('/:projectId/backlog/:id', auth.isLoggedIn, function(req, res, next) {
    backlogActions.updateBacklogItem(req,res,req.params.id);
});

/**
 * Get get certain backlog item of project
 */
router.get('/:projectId/backlog/:id', auth.isLoggedIn, function(req, res, next) {
    backlogActions.getBacklogItemById(req,res,req.params.id);
});

module.exports = router;