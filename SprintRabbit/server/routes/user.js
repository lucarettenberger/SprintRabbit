var express = require('express');
var router = express.Router();
var userActions = require('../actions/user');
var auth = require('../actions/auth');

/**
 * Get all users
 */
router.get('/', auth.isLoggedIn, function(req, res) {
    userActions.getAllUsers(req,res);
});

/**
 * Get specific user by id
 */
router.get('/:id',auth.isLoggedIn, function(req, res) {
    userActions.getUserById(req,res,req.params.id);
});

/**
 * Update own user.
 */
router.put('/', auth.isLoggedIn, function(req, res) {
    userActions.updateUser(req,res);
});

module.exports = router;