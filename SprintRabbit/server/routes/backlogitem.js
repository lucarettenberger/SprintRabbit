var express = require('express');
var router = express.Router();
var backlogItemActions = require('../actions/backlogitem');
var auth = require('../actions/auth');


/**
 * Get all backlog items
 */
router.get('/', auth.isLoggedIn, auth.isMemberOfProject, function(req, res, next) {
    backlogItemActions.getAllBacklogItems(req,res,req.body.projectId,req.body.sprintId);
});

/**
 * Get certain backlog item
 */
router.get('/:id', auth.isLoggedIn, auth.isMemberOfProject, function(req, res, next) {
    backlogItemActions.getBacklogItemById(req,res,req.params.id);
});

/**
 * create new backlog item
 */
router.post('/', auth.isLoggedIn, auth.isAdminOfProject, function(req, res, next) {
    backlogItemActions.createBacklogItem(req,res,req.body.projectId,req.body.sprintId);
});

/**
 * update certain backlog item
 */
router.put('/:id', auth.isLoggedIn, auth.isMemberOfProject, function(req, res, next) {
    backlogItemActions.updateBacklogItem(req,res,req.params.id);
});

/**
 * delete certain backlog item
 */
router.delete('/:id', auth.isLoggedIn,auth.isAdminOfProject, function(req, res, next) {
    backlogItemActions.deleteBacklogItem(req,res,req.params.id);
});


module.exports = router;