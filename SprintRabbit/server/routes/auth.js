var express = require('express');
var router = express.Router();
var passport = require('passport');
var authActions = require('../actions/auth');


/**
 * Register new user
 */
router.post('/register', function(req, res, next) {
    passport.authenticate('register', function(err, user) {
        if (err) {
            return res.status(500).json({"error-code": err || -1});
        }
        req.logIn(user, function(err) {
            if (err) {
                return res.status(500).json({"error-code": err || -1});
            }
            return res.status(201).json(user);
        });
    })(req, res, next);
});

/**
 * Login existing user
 */
router.post('/login', function(req, res, next) {
    passport.authenticate('login', function(err, user) {
        if (err) {
            return res.status(500).json({"error-code": err || -1});
        }
        req.logIn(user, function(err) {
            if (err) {
                return res.status(500).json({"error-code": err || -1});
            }
            return res.status(200).json(user);
        });
    })(req, res, next);
});


/**
 * Logout user
 */
router.post('/logout', function(req, res, next) {
    var err = authActions.logout(req);
    if(err){
        return res.status(500).json({"error-code": err || -1});
    }
    return res.status(200).end();
});

/**
 * Logout user
 */
router.get('/isLoggedIn', function(req, res, next) {
    return res.json( authActions.isAuthenticated(req))
});

module.exports = router;