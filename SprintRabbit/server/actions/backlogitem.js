/**
 *  Actions for backlog-items
 **/
const errorCodes = require('../config/errorcodes');
const BacklogItem = require('../models').BacklogItem;
const UserProject = require('../models').UserProject;
const Project = require('../models').Project;
var exports = module.exports = {};

exports.createBacklogItem = function(req,res,projectId,sprintId){
    UserProject.find({where: {project_key : projectId, user_key: req.user.id}}).then((userProject) => {
        if(userProject != null){
            if(req.body.title == undefined || req.body.title == ''){
                return res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
            }
            if(projectId == undefined ||  req.user.id == undefined){
                return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
            }
            var data = {
                title: req.body.title,
                estimatedTime: req.body.estimatedTime,
                loggedTime: req.body.loggedTime,
                description: req.body.description,
                acceptanceCriteria: req.body.acceptanceCriteria,
                status: req.body.status,
                project_key: projectId,
                sprint_key: sprintId,
                assignee_key: req.user.id
            };
            if(data.sprint_key == null){
                data.status = 0;
            }
            if(data.sprint_key != null){
                data.status = 1;
            }
            BacklogItem.create(data).then( backlogItem => {
                return res.status(201).json(backlogItem);
            }).catch(err => {
                if(err.name == "SequelizeDatabaseError"){
                    res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
                }else{
                    res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
                }
                throw new Error(err); //error throw rolls back transaction
            });
        }else{
            return res.status(401).end();
        }
    });
};

exports.getBacklogItemById = function(req,res,backlogItemId){
    BacklogItem.findById(backlogItemId)
        .then(backlogItem => {
            if(backlogItem == undefined){
                return res.status(404).json({"error-code": errorCodes.INVALID_ID});
            }
            return res.status(200).json(backlogItem);
        }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.updateBacklogItem = function(req, res,backlogItemId){
    BacklogItem.findById(backlogItemId).then(backlogItem => {
        var data = {
            title: req.body.title || backlogItem.title,
            estimatedTime: req.body.estimatedTime || backlogItem.estimatedTime,
            loggedTime: req.body.loggedTime || backlogItem.loggedTime,
            description: req.body.description || backlogItem.description,
            acceptanceCriteria: req.body.acceptanceCriteria || backlogItem.acceptanceCriteria,
            status: req.body.status || backlogItem.status,
            project_key: req.body.projectId || backlogItem.project_key,
            sprint_key: req.body.sprintId,
            assignee_key: req.body.assignee_key || backlogItem.assignee_key
        };
        if(data.sprint_key == null){
            data.status = 0;
        }
        if(data.status == 0 && data.sprint_key != null){
            data.status = 1;
        }
        backlogItem.update(data).then( result => {
            return res.status(200).json(data);
        });
    }).catch(err => {
        if(err.name == "SequelizeDatabaseError"){
            res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
        }else{
            res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        }
        throw new Error(err); //error throw rolls back transaction
    });
};

exports.deleteBacklogItem = function(req,res,backlogItemId){
    BacklogItem.destroy({where: {id : backlogItemId}}).then( deletedRows => {
        if(deletedRows == 1){
            return res.status(200).end();
        }
        if(deletedRows == 0){
            return res.status(404).json({"error-code": errorCodes.INVALID_ID});
        }
        res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err);
    });
};

exports.getAllBacklogItemsForProject = function(req,res,projectId){
    BacklogItem.findAll({where: {
            project_key : projectId
        }}).then( backlogItems => {
        UserProject.find({where: {
                project_key :
                projectId, user_key: req.user.id
        }}).then((userProject) => {
            if(userProject != null){
                return res.status(200).json(backlogItems);
            }else{
                return res.status(401).end();
            }
        });
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.getAllBacklogItems = function(req,res,projectId,sprintId){
    BacklogItem.findAll({where: {
            project_key : projectId,
            sprint_key: sprintId
        }}).then( backlogItems => {
        return res.status(200).json(backlogItems);
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};
