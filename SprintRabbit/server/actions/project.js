/**
 *  Actions for Projects
 **/

const errorCodes = require('../config/errorcodes');
const User = require('../models').User;
const Project = require('../models').Project;
const Sprint = require('../models').Sprint;
const UserProject = require('../models').UserProject;
const BacklogItem = require('../models').BacklogItem;
var exports = module.exports = {};

exports.createProject = function(req,res){
    if(req.body.name == undefined){
        return res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
    }
    UserProject.create({'role' : req.body.role || 'Scrum Master', isAdmin: true}).then( userProject => {
        User.findById(req.user.id)
            .then(user => {
                user.addUserProject(userProject);
                Project.create({'name' : req.body.name}).then ( project => {
                    project.addUserProject(userProject);
                    return res.status(201).json(project);
                })
            });
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err); //error throw rolls back transaction
    });
};

exports.getAllProjects = function(req,res,userId){
    Project.findAll(
    {  include:{
        model: UserProject,
        where: {user_key: userId}
    }}
    ).then( projects => {
        returnProjects = [];
        projects.forEach( (project) => {
            returnProjects.push({
                'id' : project.id,
                'name' : project.name,
                "createdAt": project.createdAt,
                "updatedAt": project.updatedAt,
                "role": project.UserProjects[0].role,
                "isAdmin": project.UserProjects[0].isAdmin,
                "project_key": project.UserProjects[0].project_key,
                "user_key": project.UserProjects[0].user_key,
                });
        });
        return res.status(200).json(returnProjects);
    });
};

exports.getProjectById = function(req,res,projectId){
    Project.findById(projectId)
        .then(project => {
            if(project == undefined){
                return res.status(404).json({"error-code": errorCodes.INVALID_ID});
            }
            UserProject.find({where: {project_key : projectId, user_key: req.user.id}}).then((userProject) => {
                if(userProject != null){
                    return res.status(200).json(project);
                }else{
                    return res.status(401).end();
                }
            });
        }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.deleteProject = function(req,res,projectId){
    Sprint.findAll({where: {project_key : projectId}}).then( sprints => {
        sprints.forEach(function(sprint) {
            sprint.destroy();
        });
        BacklogItem.findAll({where: {project_key : projectId}}).then( backlogItems => {
            backlogItems.forEach(function(backlogItem) {
                backlogItem.destroy();
            });
            UserProject.findAll({where: {project_key : projectId}}).then( userProjects => {
                userProjects.forEach(function(userProject) {
                    userProject.destroy();
                });
                Project.destroy({where: {id : projectId}}).then( deletedRows => {
                    if(deletedRows == 1){
                        return res.status(200).end();
                    }
                    if(deletedRows == 0){
                        return res.status(404).json({"error-code": errorCodes.INVALID_ID});
                    }
                    res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
                })
            });
        });
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err);
    });
};

exports.makeAdmin = function(req,res,userId,projectId){
    UserProject.findAll({
        where : {
            project_key: projectId,
            user_key: userId
        }
    }).then( projects => {
        if(projects.length == 0){
            return res.status(404).json({"error-code": errorCodes.INVALID_ID});
        }
        if(projects.length != 1){
            return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        }
        projects[0].update({isAdmin: true}).then( result => {
            return res.status(200).json(result);
        });
    });
};

exports.addUserToProject = function(req,res,userId,projectId){
    UserProject.create({'role' : req.body.role || 'Scrum Master', isAdmin: false}).then( userProject => {
        User.findById(userId)
            .then(user => {
                user.addUserProject(userProject);
                Project.findById(projectId).then( project => {
                    project.addUserProject(userProject);
                    return res.status(201).json(userProject);
                })
            });
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err); //error throw rolls back transaction
    });
};

exports.removeUserFromProject = function(req,res,userId,projectId){
    UserProject.destroy({
        where : {
            project_key: projectId,
            user_key: userId
        }
    }).then( deletedRows => {
        if(deletedRows == 1){
            removeProjectIfNoUser(req,res,projectId);
            return res.status(200).end();
        }
        if(deletedRows == 0){
            return res.status(404).json({"error-code": errorCodes.INVALID_ID});
        }
        res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err);
    });
};

exports.updateUserProject = function(req, res, userId, projectId){
    UserProject.findAll({
        where : {
            project_key: projectId,
            user_key: userId
        }
    }).then( userProject => {
        if(userProject.length == 0){
            return res.status(404).json({"error-code": errorCodes.INVALID_ID});
        }
        if(userProject.length != 1){
            return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        }
        var data = {
            role : req.body.role,
            isAdmin : req.body.isAdmin
        };
        userProject[0].update(data).then( result => {
            return res.status(200).json(data);
        });
    });
};

exports.getAllUsers = function(req,res,projectId){
    User.findAll(
        {  include:{
                model: UserProject,
                where: {project_key: projectId}
            }}
    ).then( users => {
        let returnUsers = [];
        users.forEach( (user) => {
            returnUsers.push({
                'id' : user.id,
                'username' : user.username,
                "firstName": user.firstName,
                "lastName": user.lastName,
                "role": user.UserProjects[0].role,
                "isAdmin": user.UserProjects[0].isAdmin,
                "project_key": user.UserProjects[0].project_key,
                "user_key": user.UserProjects[0].user_key,
            });
        });
        return res.status(200).json(returnUsers);
    });
};

function removeProjectIfNoUser(req,res,projectId){
    UserProject.findAll({
        where : {
            project_key: projectId
        }
    }).then( projects => {
        if(projects.length != 0){
            return;
        }
        exports.deleteProject(req,res,projectId);
    });
}