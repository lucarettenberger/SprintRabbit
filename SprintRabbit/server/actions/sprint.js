/**
 *  Actions for Sprints
 **/
const errorCodes = require('../config/errorcodes');
const Sprint = require('../models').Sprint;
const BacklogItem = require('../models').BacklogItem;
var exports = module.exports = {};

exports.createSprint = function(req,res,projectId){
    if(projectId == undefined || !isDateValid(req.body.startTime)
    || !isDateValid(req.body.endTime)){
        return res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
    }
    Sprint.findAll({where: {project_key : projectId}}).then( projects => {
        if(projects == undefined){
            return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        }
        //make nr of new sprint one bigger than biggest one
        var sprintNr = 0;
        projects.forEach(function(project) {
            if(project.nr > sprintNr){
                sprintNr=project.nr;
            }
        });
        sprintNr++;
        var data = {
            startTime : new Date(req.body.startTime),
            endTime : new Date(req.body.endTime),
            nr: sprintNr,
            name : req.body.name || 'Sprint Nr.'+sprintNr,
            project_key : projectId
        };
        Sprint.create(data).then( sprint => {
            return res.status(201).json(sprint);
        });
        }).catch(err => {
            throw new Error(err); //error throw rolls back transaction
            return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.updateSprint = function(req, res,sprintId){
    Sprint.findById(sprintId).then(sprint => {
        if(!isDateValid(req.body.startTime || '2001-01-01') || //if there is no date given take valid one
            !isDateValid(req.body.endTime || '2001-01-01')){
            return res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
        }
        if(req.body.startTime != undefined){
            var startTime = new Date(req.body.startTime);
        }
        if(req.body.endTime != undefined){
            var endTime = new Date(req.body.endTime);
        }
        var data = {
            startTime : startTime || sprint.startTime,
            endTime : endTime || sprint.endTime,
            nr: sprint.nr,
            name : req.body.name || sprint.name,
            project_key : sprint.project_key
        };
        sprint.update(data).then( result => {
            return res.status(200).json(data);
        });
    }).catch(err => {
        throw new Error(err); //error throw rolls back transaction
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.getAllSprints = function(req,res,projectId){
    Sprint.findAll({where: {project_key : projectId}}).then( sprints => {
        return res.status(200).json(sprints);
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err);
    });
};

exports.getSprint = function(req,res,sprintId){
    Sprint.findById(sprintId).then( sprint => {
        if(sprint == null){
            return res.status(500).json({"error-code": errorCodes.INVALID_ID});
        }
        return res.status(200).json(sprint);
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err);
    });
};

exports.deleteSprint = function (req,res,sprintId){
    BacklogItem.findAll({where: {sprint_key : sprintId}}).then( backlogItems => {
        backlogItems.forEach(function(backlogItem) {
            backlogItem.destroy();
        });
        Sprint.destroy({where: {id : sprintId}}).then( deletedRows => {
            if(deletedRows == 1){
                return res.status(200).end();
            }
            if(deletedRows == 0){
                return res.status(404).json({"error-code": errorCodes.INVALID_ID});
            }
            res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        })
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
        throw new Error(err);
    });
}


function isDateValid(date){
    if(date == undefined){
        return false;
    }
    return new Date(date) != 'Invalid Date';
}