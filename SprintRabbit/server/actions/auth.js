/**
 * Module for Passport and Session Signup / Login.
 */

const errorCodes = require('../config/errorcodes');
const userActions = require('./user');
const UserProject = require('../models').UserProject;

module.exports = function(passport, user) {

    var User = user;
    var LocalStrategy = require('passport-local').Strategy;

    passport.use('register', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            userActions.createUser({
                username: username,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                password: password,
                email: req.body.email
            },done);
        }
    ));

    passport.use('login', new LocalStrategy(
        {
            username: 'username',
            password: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback

        },
        function(req, username, password, done) {
            userActions.loginUser(username,password,done);
        }
    ));

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findByPk(id).then(function(user) {
            if (user) {
                done(null, user.get());
            } else {
                done(user.errors, null);
            }
        });
    });

};

module.exports.isLoggedIn = function (req,res,next) {
    if(req.isAuthenticated()){
        return next();
    }
    return res.status(401).end();
};


module.exports.isAuthenticated = function (req) {
    if(req.isAuthenticated()){
        return {
            authenticated: true,
            user: req.user,
        };
    }
    return {
        authenticated: false,
    };
};

module.exports.isMemberOfProject = function(req,res,next){
    var projectId = req.params.projectId || req.body.projectId;
    if(projectId == undefined || req.user.id == undefined){
        return res.status(401).end();
    }
    UserProject.findAll({
        where : {
            project_key: projectId,
            user_key: req.user.id
        }
    }).then( projects => {
        if(projects.length == 0){
            return res.status(401).end();
        }
        return next();
    });
};

module.exports.isAdminOfProject = function (req,res,next) {
    var projectId = req.params.projectId || req.body.projectId;
    if(projectId == undefined || req.user.id == undefined){
        return res.status(500).end();
    }
    UserProject.findAll({
        where : {
            project_key: projectId,
            user_key: req.user.id
        }
    }).then( projects => {
        if(projects.length == 0){
            return res.status(404).json({"error-code": errorCodes.INVALID_ID});
        }
        if(projects[0] == undefined ){
            return res.status(500).end();
        }
        if(!projects[0].isAdmin){
            return res.status(401).end();
        }
        return next();
    });
};

module.exports.logout = function (req) {
    req.session.destroy(function(err){
        if(err){
            return errorCodes.UNKNOWN_ERROR;
        }
        return null;
    });
};