/**
 *  Actions for Users
 **/

const bCrypt = require('bcrypt-nodejs');
const errorCodes = require('../config/errorcodes');
const User = require('../models').User;
var exports = module.exports = {};


exports.createUser = function(data,done) {
    if(isUserValid(data)){
        data.password = generateHash(data.password);
        User.findOrCreate({where: {username: data.username}, defaults: data})
            .spread((user, created) => {
                if(!created){
                    return done(errorCodes.USERNAME_ALREADY_TAKEN,null);
                }
                return done(null,user);
            });
    }else{
        return done(errorCodes.INVALID_INPUT,null);
    }
};

exports.loginUser = function(username,password,done){
    User.findOne({
        where: {
            username: username
        }
    }).then(function(user) {
        if (!user) {
            return done(errorCodes.USERNAME_DOES_NOT_EXIST,null);
        }
        if (!isValidPassword(user.password, password)) {
            return done(errorCodes.WRONG_PASSWORD,null);
        }
        var userInfo = user.get();
        delete userInfo['password']
        return done(null, userInfo);
    }).catch(function(err) {
        throw new Error(err); //error throw rolls back transaction
        return done(errorCodes.UNKNOWN_ERROR,null);
    });
};

exports.updateUser = function(req, res){
    if(req.body.password != undefined){
        req.body.password = generateHash(req.body.password);
    }
    User.findById(req.user.id).then(user => {
        var data = {
            username: req.body.username || user.username,
            firstName: req.body.firstName || user.firstName,
            lastName: req.body.lastName || user.lastName,
            password: req.body.password || user.password,
            email: req.body.email || user.email
        };
        if(!isUserValid(data)){
            return res.status(500).json({"error-code": errorCodes.INVALID_INPUT});
        }
        user.update(data).then( result => {
            delete data['password']; //dont send back password
            return res.status(200).json(data);
        });
    }).catch(err => {
        throw new Error(err); //error throw rolls back transaction
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.getAllUsers = function(req,res){
    User.findAll({
        attributes: ['id','username', 'firstName', 'lastName', 'email', 'createdAt', 'updatedAt']
    }).then( users => {
        return res.status(200).json(users);
    }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

exports.getUserById = function(req,res,userId){
    User.findById(userId,{
        attributes: ['id','username', 'firstName', 'lastName', 'email', 'createdAt', 'updatedAt']})
        .then(user => {
            if(user == undefined){
                return res.status(404).json({"error-code": errorCodes.INVALID_ID});
            }
            return res.status(200).json(user);
        }).catch(err => {
        return res.status(500).json({"error-code": errorCodes.UNKNOWN_ERROR});
    });
};

function isValidPassword(userpass,password){
    return bCrypt.compareSync(password, userpass);
}

function generateHash(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
}

function isUserValid(data){
    if(data.email == undefined || data.username == undefined || data.password == undefined ||
        data.firstName == undefined || data.lastName == undefined){
        return false;
    }
    if((new RegExp("^[a-zA-Z0-9_-]+$")).test(data.username) &&
        (new RegExp("^.+@.+\\..{1,3}(\\..{1,3})?$")).test(data.email)){
        return true;
    }
    return false;
}