'use strict';
module.exports = (sequelize, DataTypes) => {
  const Sprint = sequelize.define('Sprint', {
    startTime: {
        type: DataTypes.DATE,
        allowNull: false,
    },
    endTime: {
          type: DataTypes.DATE,
          allowNull: false,
      },
      nr: {
        type: DataTypes.INTEGER,
          allowNull: false
      },
      name: {
        type: DataTypes.STRING,
          allowNull: true
      }
  }, {});
  Sprint.associate = function(models) {
      Sprint.hasMany(models.BacklogItem, {
          foreignKey: 'sprint_key'
      });
  };
  return Sprint;
};