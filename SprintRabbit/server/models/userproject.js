'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserProject = sequelize.define('UserProject', {
    role: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    isAdmin: {
          type: DataTypes.BOOLEAN,
          defaultValue: false,
      },
  }, {});
  UserProject.associate = (models) => {
  };
  return UserProject;
};