'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    }
  }, {});
  User.associate = function(models) {
      User.hasMany(models.UserProject, {
          foreignKey: 'user_key'
      });
      User.hasMany(models.BacklogItem, {
          foreignKey: 'assignee_key'
      });
  };
  return User;
};