'use strict';
module.exports = (sequelize, DataTypes) => {
  const BacklogItem = sequelize.define('BacklogItem', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    estimatedTime: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    loggedTime: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    acceptanceCriteria: {
        type: DataTypes.TEXT,
        allowNull: true,
    },
    status: {
        type:   DataTypes.INTEGER,
        validate: { min: 0, max: 4 },
        allowNull: true,
        defaultValue: 0,
      }
  }, {});
  BacklogItem.associate = function(models) {
    // associations can be defined here
  };
  return BacklogItem;
};