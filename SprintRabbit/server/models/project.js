'use strict';
module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    }
  }, {});
  Project.associate = function(models) {
      Project.hasMany(models.UserProject, {
          foreignKey: 'project_key'
      });
      Project.hasMany(models.Sprint, {
          foreignKey: 'project_key'
      });
      Project.hasMany(models.BacklogItem, {
          foreignKey: 'project_key'
      });
  };
  return Project;
};