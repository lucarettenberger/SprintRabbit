'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * addColumn "name" to table "Sprints"
 * addColumn "nr" to table "Sprints"
 *
 **/

var info = {
    "revision": 3,
    "name": "editSprint",
    "created": "2019-01-29T11:30:47.741Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "Sprints",
            "name",
            {
                "type": Sequelize.STRING,
                "field": "name",
                "allowNull": true
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "Sprints",
            "nr",
            {
                "type": Sequelize.INTEGER,
                "field": "nr",
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
