'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * changeColumn "estimatedTime" on table "BacklogItems"
 *
 **/

var info = {
    "revision": 4,
    "name": "estTimeOpt",
    "created": "2019-01-29T16:05:40.553Z",
    "comment": ""
};

var migrationCommands = [{
    fn: "changeColumn",
    params: [
        "BacklogItems",
        "estimatedTime",
        {
            "type": Sequelize.INTEGER,
            "field": "estimatedTime",
            "allowNull": true
        }
    ]
}];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
