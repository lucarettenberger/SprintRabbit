'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * removeColumn "nr" from table "Sprints"
 * addColumn "startTime" to table "Sprints"
 * addColumn "endTime" to table "Sprints"
 *
 **/

var info = {
    "revision": 2,
    "name": "startEndForSprint",
    "created": "2019-01-29T11:00:58.378Z",
    "comment": ""
};

var migrationCommands = [{
        fn: "addColumn",
        params: [
            "Sprints",
            "startTime",
            {
                "type": Sequelize.DATE,
                "field": "startTime",
                "allowNull": false
            }
        ]
    },
    {
        fn: "addColumn",
        params: [
            "Sprints",
            "endTime",
            {
                "type": Sequelize.DATE,
                "field": "endTime",
                "allowNull": false
            }
        ]
    }
];

module.exports = {
    pos: 0,
    up: function(queryInterface, Sequelize)
    {
        var index = this.pos;
        return new Promise(function(resolve, reject) {
            function next() {
                if (index < migrationCommands.length)
                {
                    let command = migrationCommands[index];
                    console.log("[#"+index+"] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                }
                else
                    resolve();
            }
            next();
        });
    },
    info: info
};
