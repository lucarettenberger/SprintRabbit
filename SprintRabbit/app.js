var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var session = require('express-session');
var cors = require('cors');


var indexRouter = require('./server/routes/index');
var authRouter = require('./server/routes/auth');
var userRouter = require('./server/routes/user');
var backlogItemRouter = require('./server/routes/backlogitem');
var projectRouter = require('./server/routes/project');
var sprintRouter = require('./server/routes/sprint');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// For Passport
app.use(session({
    cookieName: 'sessionName',
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true,
    httpOnly: false,

})); // session secret

app.use(passport.initialize());

app.use(passport.session({
    cookieName: 'sessionName',
    secret: 'keyboard cat',
    saveUninitialized: true,
    httpOnly: false,
})); // persistent login sessions



//load passport strategies
var models = require("./server/models/");
require('./server/actions/auth')(passport, models.User);

var projectRoute = '/project';

app.use(cors({origin: 'http://localhost:8080',credentials:true,httpOnly:false}));

app.use('/auth',authRouter);
app.use('/user', userRouter);
app.use(projectRoute, projectRouter);
app.use(projectRoute+'/:projectId/sprint',saveProjectIdToBody , sprintRouter);
app.use(projectRoute+'/:projectId/sprint/:sprintId/backlog/',saveProjectIdToBody, saveSprintToBody, backlogItemRouter);
app.use('/', indexRouter);

/**
 * Put projectId into body, so it can be accesed from the underlying routers
 */
function saveProjectIdToBody (req,res,next){
    req.body.projectId = req.params.projectId;
    next();
};

/**
 * Put projectId into body, so it can be accesed from the underlying routers
 */
function saveSprintToBody (req,res,next){
    req.body.sprintId = req.params.sprintId;
    next();
};

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
